import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import BookCreate from './books/BookCreate';
import BookDelete from './books/BookDelete';
import BookEdit from './books/BookEdit';
import BookList from './books/BookList';
import BookShow from './books/BookShow';
import Header from './Header';
import history from '../history';


const App = () => {
    return (
        <div className="ui container">
            <Router history={history}>
                <div>
                    <Header />
                    <Switch>
                        <Route path="/" exact component={BookList} />
                        <Route path="/books/new"  component={BookCreate} />
                        <Route path="/books/edit/:id"  component={BookEdit} />
                        <Route path="/books/delete/:id"  component={BookDelete} />
                        <Route path="/books/:id"  component={BookShow} />
                    </Switch>
                </div>
            </Router>
        </div>
    )
}

export default App;