import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchBooks } from '../../actions';


class BookList extends React.Component{
    componentDidMount() {
        this.props.fetchBooks();
    }

    renderDeleteEdit(book){
        if(book.userId === this.props.currentUserId){
            return (
                <div className="right floated content">
                    <Link to={`/books/edit/${book.id}`} className="ui button primary">Edit</Link>
                    <Link to={`/books/delete/${book.id}`} className="ui button negative">Delete</Link>
                </div>
            )
        }
    }

    renderList(){
        return this.props.books.map(book => {
            const {id, title, description} = book;
            return (
                <div className="item" key={id}>
                    {this.renderDeleteEdit(book)}
                    <i className="large middle aligned icon book"/>
                    <div className="content">
                        <Link className="header" to={`/books/${id}`}>{title}</Link>
                        <div className="description">{description}</div>
                    </div>
                </div>
            )
        })
    }

    renderCreate(){
        if(this.props.isSignedIn){
            return(
               <div style={{textAlign: 'right'}}>
                   <Link to="/books/new" className="ui button primary">Add Book</Link>
               </div> 
            )
        }
    }

    render(){
        return (
            <div>
                <h2>Share books that you love</h2>
                <div className="ui celled list">
                    {this.renderList()}
                </div>
                {this.renderCreate()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        books: Object.values(state.books),
        currentUserId: state.auth.userId,
        isSignedIn: state.auth.isSignedIn
    };
}

export default connect(mapStateToProps, { fetchBooks })(BookList);