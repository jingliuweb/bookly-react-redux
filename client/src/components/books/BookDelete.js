import React from 'react';
import Modal from '../Modal';
import history from '../../history';
import { connect } from 'react-redux';
import { fetchBook, deleteBook } from '../../actions';

class BookDelete extends React.Component {
    componentDidMount() {
        this.props.fetchBook(this.props.match.params.id);
    }

    renderActions = () => {
        return(
            <React.Fragment>
                <button onClick={this.onDelete} className="ui button negative">Delete</button>
                <button onClick={this.onDismiss} className="ui button">Cancel</button>
            </React.Fragment>
        );
    }

    onDelete = () => {
        this.props.deleteBook(this.props.match.params.id);
    }

    onDismiss = () => history.push('/');

    renderContent(){
        if(!this.props.book){
            return 'Are you sure you want to delete this book?'
        }

        return `Are you sure you want to delete: ${this.props.book.title}?`
    }
     
    render(){
        return (
            <Modal 
                title="Delete Book"
                content={this.renderContent()}
                actions={this.renderActions()}
                onDismiss={this.onDismiss}
            />
        );
    }
    
}

const mapStateToProps = (state, ownProps) => {
    return { book: state.books[ownProps.match.params.id]}
}

export default connect(mapStateToProps, {fetchBook, deleteBook})(BookDelete);